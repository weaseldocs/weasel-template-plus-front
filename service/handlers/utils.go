package handlers

import (
	"context"
	"os"

	"github.com/jackc/pgx/v4/pgxpool"
)

var DBPool *pgxpool.Pool

func InitDB() (*pgxpool.Pool, error) {
	return pgxpool.Connect(context.Background(), os.Getenv(""))
}
