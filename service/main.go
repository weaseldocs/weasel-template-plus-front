package main

import (
	"context"
	"fmt"
	"log"
	"modulename/handlers"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func main() {
	gin.SetMode(gin.ReleaseMode)
	r := gin.New()
	r.Use(gin.Recovery())
	r.Use(cors.New(cors.Config{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{"POST", "OPTIONS"},
		AllowHeaders: []string{"Origin", "Content-Type", "X-Auth-Token", "Authorization", "X-Requested-With"},
	}))

	r.POST("/sample", handlers.Sample)

	s := &http.Server{
		Addr:         ":9090",          // configure the bind address
		Handler:      r,                // set the default handler
		ReadTimeout:  10 * time.Second, // max time to read request from the client
		WriteTimeout: 10 * time.Second, // max time to write response to the client
	}

	go func() {
		fmt.Println("Web service running in port 9090")
		var err error
		// Uncomment next lines if you need a DB pool, and check parameters in handlers/utils.go file
		// handlers.DBPool, err = handlers.InitDB()
		// if err != nil {
		// 	fmt.Printf("Can't connect to DB: %s\n", err)
		// 	os.Exit(1)
		// }
		err = s.ListenAndServe()
		if err != nil {
			fmt.Printf("Error starting server: %s\n", err)
			os.Exit(1)
		}
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, syscall.SIGINT, syscall.SIGTERM, os.Interrupt)
	// Block until a signal is received.
	sig := <-c
	log.Println("Got signal:", sig)
	// gracefully shutdown the server, waiting max 30 seconds for current operations to complete
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	if err := s.Shutdown(ctx); err != nil {
		log.Fatal("Server forced to shutdown:", err)
	}
	log.Println("Server shutdown")
}
